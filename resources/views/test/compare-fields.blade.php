@extends('layouts.basic')

@section('content')
    <h3>Choose your mapping: </h3>
    <form method="POST" action="{{route('post-data')}}">
        @csrf
        @foreach($dbColumns as $dbColumn)
            <div class="form-group" style="display: flex; justify-content: space-between;">
                {{$dbColumn}}
                    <select id="{{$dbColumn}}" name="{{$dbColumn}}">
                        <option value="">-</option>
                        @foreach($fileColumns as $key => $fileColumn)
                            <option value="{{$fileColumn}}">
                                {{$fileColumn}}
                            </option>
                        @endforeach
                    </select>
            </div>
            <hr>
        @endforeach
        <input type="hidden" name="fileName" value="{{$fileName}}">
        <button class="btn btn-dark" type="submit">Proceed</button>
    </form>
@endsection
