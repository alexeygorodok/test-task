@extends('layouts.basic')

@section('content')
    <form method="POST" action="{{route('read-file')}}" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="exampleFormControlFile1">Example file input</label>
            <input type="file" class="form-control-file" name="file">
            <button class="btn btn-dark" type="submit">Send</button>
        </div>
    </form>
@endsection
