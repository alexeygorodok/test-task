<?php

namespace App\Services;

use App\Models\Product;
use Carbon\Carbon;

class ProcessCSVService {

    public function readCSV($csvFile)
    {
        $file_handle = fopen($csvFile, 'r');
        while (!feof($file_handle)) {
            $line_of_text[] = fgetcsv($file_handle, 0, ',');
        }
        fclose($file_handle);
        return $line_of_text;
    }

    public function readCSVFromContent($file_handle)
    {
        $lines = explode(PHP_EOL, $file_handle);
        $array = [];
        foreach ($lines as $line) {
            $array[] = str_getcsv($line);
        }
        return $array;
    }

    public function readCSVheader($csvFile) {
        $rows = $this->readCSV($csvFile);

        return $rows[0];
    }

    public function processData($request, $file) {
        $mappedFields = $request->all();
        $csvArrayData = $this->readCSVFromContent($file);
        $csvFields = $csvArrayData[0];
        $products = [];

        for ($i = 1; $i < count($csvArrayData)-1; $i++)
        {
            $csvProductData = $csvArrayData[$i];

            foreach ($csvProductData as $key => $productDataField) {
                $productObjectWithFieldName[$csvFields[$key]] = $productDataField;
            }

            $product = new Product();

            $product->brand = $productObjectWithFieldName[$mappedFields['brand']];
            $product->owner = $productObjectWithFieldName[$mappedFields['owner']];
            $product->price = $productObjectWithFieldName[$mappedFields['price']];
            $product->description = $productObjectWithFieldName[$mappedFields['description']];
            $product->url = $productObjectWithFieldName[$mappedFields['url']];
            $product->date = Carbon::createFromTimestamp($productObjectWithFieldName[$mappedFields['date']])->toDateTimeString();
            $product->model = ($mappedFields['model']) ? $productObjectWithFieldName[$mappedFields['model']] : null;
            $product->product_id = ($mappedFields['product_id']) ? $productObjectWithFieldName[$mappedFields['product_id']] : null;
            $product->country = ($mappedFields['country']) ? $productObjectWithFieldName[$mappedFields['country']] : null;
            $product->store_id = ($mappedFields['store_id']) ? $productObjectWithFieldName[$mappedFields['store_id']] : null;
            $product->active = ($mappedFields['active']) ? (integer) $productObjectWithFieldName[$mappedFields['active']] : null;

            $product->save();

            $products[] = $product;
        }

        return $products;
    }
}
