<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = 'products';

    protected $fillable = [
      'brand',
      'owner',
      'price',
      'description',
      'url',
      'date',
      'model',
      'product_id',
      'country',
      'store_id',
      'active',
    ];

    protected $hidden = ['created_at', 'updated_at'];
}
