<?php

namespace App\Http\Controllers;

use App\Services\ProcessCSVService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class TestController extends Controller
{
    private $csvService;

    public function __construct(ProcessCSVService $processCSVService)
    {
        $this->csvService = $processCSVService;
    }

    public function index()
    {
        return view('test.index');
    }

    public function readFile(Request $request)
    {
        $file = $request->file('file');

        $directoryName = storage_path('app/files');
        $fileName = $file->getClientOriginalName();

        if(!\File::isDirectory($directoryName)) {
            \Storage::makeDirectory($directoryName);
        }

        $fileColumns = $this->csvService->readCSVheader($file);

        Cache::put('csv_file'.$file->getClientOriginalName(), $file->getContent(), 1440);

        $dbColumns = \Arr::except(\Schema::getColumnListing('products'), [12, 13]);

        return view('test.compare-fields', [
            'fileColumns' => $fileColumns,
            'dbColumns' => $dbColumns,
            'fileName' => $fileName
        ]);
    }

    public function postAssociatedData(Request $request) {

        try {
            $fileName = $request->get('fileName');

            $request->request->remove('_token');
            $request->request->remove('fileName');
            $request->request->remove('id');

            $file = Cache::get('csv_file'.$fileName);
            Cache::forget('csv_file'.$fileName);

            $products = $this->csvService->processData($request, $file);

            return view('test.result', ['products' => $products]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }

}
