<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */


    //  Table csv difference

//id            id              id
//brand 	    publisher	    publisher
//variant	    type		    NULL                 nullable
//name  	    name	        name
//price 	    price	        price
//url	        product_url	    product_url
//product_id	content_id		NULL                 nullable
//created_at	date	        date
//description	description 	description
//NULL          country		    NULL                 nullable
//NULL          store_id		NULL                 nullable
//NULL          active      	online		         nullable

    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('brand');
            $table->string('owner');
            $table->string('price');
            $table->longText('description');
            $table->text('url');
            $table->timestamp('date');

//      Nullable fields
            $table->string('model')->nullable();
            $table->string('product_id')->nullable();
            $table->string('country')->nullable();
            $table->string('store_id')->nullable();
            $table->boolean('active')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
