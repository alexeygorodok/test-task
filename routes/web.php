<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\TestController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [TestController::class, 'index']);

Route::get('/test-task', [TestController::class, 'index'])->name('test-index');
Route::post('/read-file', [TestController::class, 'readFile'])->name('read-file');
Route::post('/post-data', [TestController::class, 'postAssociatedData'])->name('post-data');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
